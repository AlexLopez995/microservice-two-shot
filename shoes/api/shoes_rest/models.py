from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.closet_name


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=30)
    model_name = models.CharField(max_length=30)
    color = models.CharField(max_length=30)
    picture_url = models.URLField(null=True, blank=True)
    bin = models.ForeignKey(
        BinVO,
        related_name= "bins",
        on_delete= models.CASCADE,
    )

    def __str__(self):
        return self.model_name

    def get_api_url(self):
        return reverse("api_shoe_details", kwargs={"pk": self.pk})

import { useEffect, useState } from 'react';
import './index.css';

function HatsList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
            console.log(data.hats)
        }
    }

    useEffect(()=>{
        getData()
      }, [])

      const deleteHat = async(id) => {
        const fetchConfig = {
          method: "delete",
          headers: {
            'Content-Type': 'application/json',
          },
        };
        await fetch(`http://localhost:8090/api/hats/${id}`, fetchConfig)
        setHats(prevHats => prevHats.filter(hat => hat.id !== id));
      };

      return (
          <table className="table">
            <thead>
              <tr>
                <th>Fabric</th>
                <th>Style</th>
                <th>Color</th>
                <th>Photo</th>
                <th>Location</th>
              </tr>
            </thead>
            <tbody>
              {console.log(hats)}
              {hats.map(hat => {
                return (
                  <tr key={hat.id}>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.style }</td>
                    <td>{ hat.color }</td>
                    <td><img className="hat-image" src={ hat.url } alt="hat" /></td>
                    <td>{ hat.location.closet_name } / section { hat.location.section_number } / shelf { hat.location.shelf_number }</td>
                    <td><button className="btn btn-primary" type="button" id={hat.id} onClick={() => deleteHat(hat.id)}>Delete</button></td>
                  </tr>
                );
              })}
            </tbody>
          </table>
      );

}

export default HatsList;

import { useEffect, useState } from "react";

function ShoesList() {
    const [shoes, setShoes] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/');

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
            console.log(data.shoes)
        }
    }
    useEffect(()=>{
        getData()
    }, [])
    const deleteShoe = async(href) => {
        const fetchConfig = {
            method: "DELETE",
            "Content-Type" : "application/json",
        }
        await fetch(`http://localhost:8080/${href}`, fetchConfig)
        setShoes(prevShoes => prevShoes.filter(shoe => shoe.href !== href))
    }
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model Name</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Bin</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map(shoe => {
              return (
                <tr key={shoe.id}>
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.model_name }</td>
                  <td>{ shoe.color }</td>
                  <td>
                    <img src={shoe.picture_url} className="card-img-top" alt=""/>
                  </td>
                  <td>{ shoe.bin.closet_name }</td>
                  <td>
                    <button type="button" id={shoe.id} onClick={() => deleteShoe(shoe.href)} className="btn btn-danger">Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
}

export default ShoesList;

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './Shoeslist';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import ShoesForm from './ShoesForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatsList />} />
            <Route path="new" element={<HatsForm />} />
          // </Route>
          <Route path="shoes">
            <Route index element={<ShoesList />} />
            <Route path="new" element={<ShoesForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

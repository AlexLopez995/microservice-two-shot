import { useEffect, useState } from 'react';

function HatsForm() {

    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
        fabric: '',
        style: '',
        color: '',
        url: '',
        location: '',
    })

    const getLocationData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
          console.log(data.locations);
        }
      }

      useEffect(()=> {
        getLocationData();
      }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const hatsUrl = 'http://localhost:8090/api/hats/';

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        console.log(fetchConfig)
        const response = await fetch(hatsUrl, fetchConfig);

        if (response.ok) {
            event.target.reset();
            setFormData({
                fabric: '',
                style: '',
                color: '',
                url: '',
                location: '',
            });
        }
    };

        const handleChangeInput = (e) => {
            const value = e.target.value;
            const inputName = e.target.name;
            setFormData({
              ...formData,
              [inputName]: value
            });
          }

    return (
        <div className="row">
            <div className="col-8 col-md-6 col-lg-3">
                <h1>Add a Hat</h1>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label htmlFor="fabric">Fabric</label>
                        <input onChange={handleChangeInput} required placeholder="Fabric" type="text" id="fabric" name="fabric" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor="fabric">Style</label>
                        <input onChange={handleChangeInput} required placeholder="Style" type="text" id="style" name="style" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor="fabric">Color</label>
                        <input onChange={handleChangeInput} required placeholder="Color" type="text" id="color" name="color" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor="fabric">Photo URL</label>
                        <input onChange={handleChangeInput} required placeholder="Photo URL" type="text" id="url" name="url" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor="location">Location</label>
                        <select className="form-control" onChange={handleChangeInput} name="location" id="location" required>
                        <option value="" >Choose a closet</option>
                        {
                        locations.map(location => {
                            return (
                            <option key={location.href} value={location.href}>{location.closet_name}</option>
                            )
                        })
                        }
                    </select>

                    </div>
                    <button className="btn btn-primary mt-3">Submit</button>

                </form>
            </div>
        </div>
    );

}

export default HatsForm;
